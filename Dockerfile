FROM ubuntu:bionic

# We can set the git branch to get by using --build-arg when we use docker build.
ARG BRANCH=master

# Create the home directory for the new worker user.
RUN mkdir -p /home/worker

# Create work and tool dirs.
RUN mkdir -p /work/tools

# Change symlink for bash.
RUN cd /bin/ && rm sh && ln -s bash sh

# Create an worker user so our program doesn't run as root.
RUN groupadd worker && useradd -u 8877 -s /bin/bash -g worker -m -k /dev/null worker

RUN chown worker:worker -R /work

# Update and install packages.
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y && DEBIAN_FRONTEND=noninteractive apt-get install texi2html autoconf-archive curl bison g++ xz-utils libtool automake pkg-config perl libtool m4 autoconf gawk build-essential texinfo tree git sudo p7zip lzma-dev lzma wget coreutils -y && apt-get autoremove -y

RUN DEBIAN_FRONTEND=noninteractive apt-get install libmpfr-dev libmpc-dev libgmp-dev ncurses-base libperl-dev libattr1-dev -y && apt-get clean && apt-get autoremove -y

RUN sync

# Run LFS check.
RUN wget https://gist.githubusercontent.com/junland/ebdfd64b1a0862b68d34c24e38693712/raw/475fcddd970a608cff7dbe8965718dee229ddf84/version-check.sh
RUN chmod +x ./version-check.sh
RUN ./version-check.sh && sleep 30

# Pull mktools repo and ready script.
RUN git clone https://gitlab.com/gemstone-linux/mktools.git && cd /mktools && git pull origin $BRANCH
RUN cd /mktools && make env-docker
RUN chown worker:worker -R /mktools/*
RUN cp /mktools/.bashrc /home/worker/.bashrc
RUN chown worker:worker -R /home/worker
WORKDIR /mktools
RUN chmod 777 mktools

# Create tools dir.
RUN mkdir -p /work/tools
RUN chown worker:worker -R /work/tools && chmod -v a+wt /work/tools

# Create sources dir.
RUN mkdir -p /work/sources
RUN chown worker:worker -R /work/sources && chmod -v a+wt /work/sources

# Create link to tools.
RUN ln -sv /work/tools /

# Change permissions for link.
RUN chmod 777 /tools
RUN chown worker:worker /tools

# Inject .bashrc for worker user.
RUN echo "set +h" > /home/worker/.bashrc
RUN echo "umask 022" >> /home/worker/.bashrc
RUN echo "WORK=/work" >> /home/worker/.bashrc
RUN echo "TOOLS=$WORK/tools" >> /home/worker/.bashrc
RUN echo "SOURCES=$WORK/sources" >> /home/worker/.bashrc
RUN echo "LOGS_DIR=$WORK/logs" >> /home/worker/.bashrc
RUN echo "TOOLS_TGT=$(uname -m)-project_gemstone-linux-gnu" >> /home/worker/.bashrc
RUN echo "PATH=/tools/bin:/bin:/usr/bin" >> /home/worker/.bashrc
RUN echo "TOOLS_STEPS_DIR=$(pwd)/tools" >> /home/worker/.bashrc
RUN echo "MAKEFLAGS="-j1"" >> /home/worker/.bashrc
RUN echo "export WORK TOOLS SOURCES LOGS_DIR TOOLS_TGT PATH TOOLS_STEPS_DIR MAKEFLAGS" >> /home/worker/.bashrc
RUN chown worker:worker /home/worker/.bashrc
RUN chmod 777 /home/worker/.bashrc

# Sync everything.
RUN sync

# Prepare non-root user to run mktools.
USER worker
ENV MKTOOLS_LIB=/mktools/lib/libmktools.sh
WORKDIR /mktools

ENTRYPOINT ["/mktools/mktools"]
