VAGRANTFILE_API_VERSION = "2"

Vagrant.configure("2") do |config|

    config.vm.box = "generic/ubuntu1804"
    config.vm.hostname = "project-gemstone"
    config.ssh.forward_agent = true
    config.vm.synced_folder ".", "/vagrant", disabled: false

  
    config.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--memory", 4048]
      # enable serial port just in case image does not
      v.customize ["modifyvm", :id, "--uart1", "0x3F8", 4]
  
      file_to_disk = 'data-disk.vdi'
      unless File.exist?(file_to_disk)
        v.customize ['createhd', '--filename', file_to_disk, '--size', 85 * 1024]
      end
      v.customize ['storageattach', :id, '--storagectl', 'IDE Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
    end

    config.vm.provider "libvirt" do |v|
      v.memory = 4048
      v.cpus = 2
      v.storage :file, :size => '40G'
    end

    config.vm.provision "shell", inline: <<-SHELL
    echo "=====> Preping external data scratch disk..."
    mkfs.ext4 /dev/sdb
    mkdir /mnt/data
    echo "/dev/sdb           /mnt/data    ext4       rw,defaults         0      0" >> /etc/fstab
    mount -a
	
    # Install dependencies
    echo "=====> Installing dependencies..."
    DEBIAN_FRONTEND=noninteractive apt update && DEBIAN_FRONTEND=noninteractive apt upgrade -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y autoconf-archive curl bison g++ xz-utils libtool automake pkg-config perl libtool m4 autoconf gawk build-essential texinfo tree git sudo wget coreutils libperl-dev -y && apt-get autoremove
    DEBIAN_FRONTEND=noninteractive apt-get install libmpfr-dev libmpc-dev pigz libgmp-dev ncurses-base libperl-dev libattr1-dev -y

    # Update dash symlink
    cd /bin/ && sudo rm sh && sudo ln -s bash sh

    # Cloneing repo
    echo "=====> Cloneing repo..."
    cd /home/vagrant && git clone https://github.com/project-gemstone/mktools && chown vagrant:vagrant -R /home/vagrant/
    curl https://gist.githubusercontent.com/junland/f4567812d4f8075c01465f501027dcac/raw/6b004251bdf1617642d3afa738de8f0d2255a5dd/vchroot.sh > /usr/local/bin/vchroot
    chmod 777 /usr/local/bin/vchroot

    # Make some dirs.
    echo "=====> Create dirs and symlinks..."
    mkdir -p /mnt/data/project-v/rootfs
    mkdir -p /mnt/data/project-v/rootfs/sources
    mkdir -p /mnt/data/project-v/rootfs/tools
    ln -sv /mnt/data/tools /
    echo "dash dash/sh boolean false" | debconf-set-selections && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

    # Change permissions.
    echo "=====> Changing permissions..."
    chown vagrant:vagrant -R /mnt/data
    chown vagrant:vagrant -R /mnt/data/*
    chown vagrant:vagrant -R /tools

    SHELL
  
  end
