env:
	echo "set +h" >> mktools.env
	echo "umask 022" >> mktools.env
	echo "WORK=" >> mktools.env
	echo "TOOLS=$WORK/tools" >> mktools.env
	echo "SOURCES=$WORK/sources" >> mktools.env
	echo "LOGS_DIR=$WORK/logs" >> mktools.env
	echo "TOOLS_TGT=$(uname -m)-project_gemstone-linux-gnu" >> mktools.env
	echo "PATH=/tools/bin:/bin:/usr/bin" >> mktools.env
	echo "TOOLS_STEPS_DIR=$(pwd)/tools" >> mktools.env
	echo "MAKEFLAGS="-j1"" >> mktools.env
	echo "export TOOLS SOURCES LOGS_DIR TOOLS_TGT PATH TOOLS_STEPS_DIR MAKEFLAGS" >> mktools.env

env-vagrant:
	echo "set +h" >> mktools.env
	echo "umask 022" >> mktools.env
	echo "WORK=/mnt/data" >> mktools.env
	echo "TOOLS=$WORK/tools" >> mktools.env
	echo "SOURCES=$WORK/sources" >> mktools.env
	echo "LOGS_DIR=$WORK/logs" >> mktools.env
	echo "TOOLS_TGT=$(uname -m)-project_gemstone-linux-gnu" >> mktools.env
	echo "PATH=/tools/bin:/bin:/usr/bin" >> mktools.env
	echo "TOOLS_STEPS_DIR=$(pwd)/tools" >> mktools.env
	echo "MAKEFLAGS="-j1"" >> mktools.env
	echo "export TOOLS SOURCES LOGS_DIR TOOLS_TGT PATH TOOLS_STEPS_DIR MAKEFLAGS" >> mktools.env
	
env-docker:
	echo "set +h" > .bashrc
	echo "umask 022" >> .bashrc
	echo "WORK=/work" >> .bashrc
	echo "TOOLS=$WORK/tools" >> .bashrc
	echo "SOURCES=$WORK/sources" >> .bashrc
	echo "LOGS_DIR=$WORK/logs" >> .bashrc
	echo "TOOLS_TGT=$(uname -m)-project_gemstone-linux" >> .bashrc
	echo "PATH=/tools/bin:/bin:/usr/bin" >> .bashrc
	echo "TOOLS_STEPS_DIR=$(pwd)/tools" >> .bashrc
	echo "MAKEFLAGS="-j1"" >> .bashrc
	echo "export TOOLS SOURCES LOGS_DIR TOOLS_TGT PATH TOOLS_STEPS_DIR MAKEFLAGS" >> .bashrc
