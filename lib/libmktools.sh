#!/bin/bash -e

## Common colors
RESTORE=$(echo -en '\033[0m') # This will terminate the color variables.
RED=$(echo -en '\033[00;31m')
GREEN=$(echo -en '\033[00;32m')
YELLOW=$(echo -en '\033[00;33m')
BLUE=$(echo -en '\033[00;34m')
WHITE=$(echo -en '\033[01;37m')

## Common functions

# print_info prints normal text
print_info() {
    if [ "$1" == "" ] ; then
        echo "No informational message has been passed... Please fix this."
        exit 99
    fi
    
    echo "=====> $1"
}

# print_err prints red text
print_err() {
    if [ "$1" == "" ] ; then
        echo "No error message has been passed... Please fix this."
        exit 99
    fi
    
    echo "=====> ${RED} $1 ${RESTORE}"
}

# print_warn prints yellow text
print_warn() {
    if [ "$1" == "" ] ; then
        echo "No warning message has been passed... Please fix this."
        exit 99
    fi
    
    echo "=====> ${YELLOW} $1 ${RESTORE}"
}

# print_warn prints yellow text
print_ok() {
    if [ "$1" == "" ] ; then
        echo "No OK message has been passed... Please fix this."
        exit 99
    fi
    
    echo "=====> ${GREEN} $1 ${RESTORE}"
}

download() {
    print_info "Downloading $1 to $2"

    curl -C - --fail --ftp-pasv --retry 4 --retry-delay 5 -L "$1" --output "$2"
}

extract() {
    print_info "Extracting $1 to $2"

    tar -xaf "$1" --strip-components 1 --directory "$2"
}

export -f print_info
export -f print_ok
export -f print_warn
export -f print_err
export -f download
export -f extract